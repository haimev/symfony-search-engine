<?php

namespace SearchBundle;

use SearchBundle\Compiler\SearchEngineCompilerPass;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SearchBundle
 * in build we add compilerPass, give us an opportunity to manipulate other service definitions
 * that have been registered with the service container
 * @package SearchBundle
 */
class SearchBundle extends Bundle
{
	/**
	 * @param ContainerBuilder $container
	 */
	public function build(ContainerBuilder $container)
	{
		// load default services.yml
		$loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/Resources/config'));
		$loader->load('services.yml');
		parent::build($container);
		$container->addCompilerPass(new SearchEngineCompilerPass());
	}
}
