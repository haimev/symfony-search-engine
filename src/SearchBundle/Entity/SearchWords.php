<?php

namespace SearchBundle\Entity;

/**
 * SearchWords
 */
class SearchWords
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $searchTerm;

    /**
     * @var \DateTime
     */
    private $ttl = 'CURRENT_TIMESTAMP';

	/**
	 * SearchWords constructor.
	 */
	public function __construct()
	{
		$this->ttl = new \DateTime();
	}

	/**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set searchTerm
     *
     * @param string $searchTerm
     *
     * @return SearchWords
     */
    public function setSearchTerm($searchTerm)
    {
        $this->searchTerm = $searchTerm;

        return $this;
    }

    /**
     * Get searchTerm
     *
     * @return string
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

    /**
     * Set ttl
     *
     * @param \DateTime $ttl
     *
     * @return SearchWords
     */
    public function setTtl($ttl)
    {
        $this->ttl = $ttl;

        return $this;
    }

    /**
     * Get ttl
     *
     * @return \DateTime
     */
    public function getTtl()
    {
        return $this->ttl;
    }
}
