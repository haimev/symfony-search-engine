<?php

namespace SearchBundle\Entity;

/**
 * SearchResults
 */
class SearchResults
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $result;

    /**
     * @var \SearchBundle\Entity\SearchWords
	 * @ManyToOne(targetEntity="SearchWords")
	 * @JoinColumn(name="search_words_id", referencedColumnName="id")
	 */
    private $searchWords;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return SearchResults
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set searchWords
     *
     * @param \SearchBundle\Entity\SearchWords $searchWords
     *
     * @return SearchResults
     */
    public function setSearchWords(\SearchBundle\Entity\SearchWords $searchWords = null)
    {
        $this->searchWords = $searchWords;

        return $this;
    }

    /**
     * Get searchWords
     *
     * @return \SearchBundle\Entity\SearchWords
     */
    public function getSearchWords()
    {
        return $this->searchWords;
    }
    /**
     * @var string
     */
    private $resultHash;


    /**
     * Set resultHash
     *
     * @param string $resultHash
     *
     * @return SearchResults
     */
    public function setResultHash($resultHash)
    {
        $this->resultHash = $resultHash;

        return $this;
    }

    /**
     * Get resultHash
     *
     * @return string
     */
    public function getResultHash()
    {
        return $this->resultHash;
    }
    /**
     * @var string
     */
    private $url;


    /**
     * Set url
     *
     * @param string $url
     *
     * @return SearchResults
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $title;


    /**
     * Set link
     *
     * @param string $link
     *
     * @return SearchResults
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return SearchResults
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
