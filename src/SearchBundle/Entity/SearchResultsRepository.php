<?php

namespace SearchBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class SearchResultsRepository
 * @package SearchBundle\Entity
 */
class SearchResultsRepository extends EntityRepository
{
	/**
	 * @param $searchName
	 * @return array
	 */
	public function getResultsBySearchName($searchName)
	{
		return $this->getEntityManager()
				->createQueryBuilder()
				->select('sr.title', 'sr.link')
				->from('SearchBundle:SearchResults', 'sr')
				->innerJoin('sr.searchWords','sw')
				->where('sw.searchTerm = :search_term')
				->setParameter('search_term', $searchName)
				->getQuery()
				->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}

	/**
	 * @param $searchWordId
	 * @param $results
	 */
	public function insertResults($searchWordId, $results)
	{
		// first delete exists results
		$this->getEntityManager()
			->createQuery('DELETE FROM SearchBundle:SearchResults sr
					WHERE sr.searchWords = :searchWordId'
			)
			->setParameter('searchWordId', $searchWordId)
			->execute();

		$em = $this->getEntityManager();

		$searchWordObj = $searchWord = $this->getEntityManager()
			->getRepository('SearchBundle\Entity\SearchWords')
			->find($searchWordId);

		// insert results
		$batchSize = 20;
		$i = 1;
		foreach ($results as $result) {

			$searchResult = new SearchResults();
			$searchResult->setLink($result['link']);
			$searchResult->setTitle($result['title']);
			$searchResult->setSearchWords($searchWordObj);

			$em->persist($searchResult);

			if (($i % $batchSize) === 0) {
				$em->flush();
			}
			$i++;
		}

		$em->flush(); //Persist objects that did not make up an entire batch
		$em->clear();
	}
}