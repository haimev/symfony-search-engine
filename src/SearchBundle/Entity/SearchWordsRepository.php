<?php

namespace SearchBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\setMaxResults;

/**
 * Class SearchWordsRepository
 * @package SearchBundle\Entity
 */
class SearchWordsRepository extends EntityRepository
{
	/**
	 * @param $searchName
	 * @param $ttl
	 * @return mixed
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function findSearchTermByTtl($searchName, $ttl)
	{
		$date = new \DateTime();

		$date->sub(new \DateInterval('PT' . $ttl . 'S'));

		return $this->getEntityManager()
				->createQuery('SELECT sw FROM SearchBundle:SearchWords sw
					WHERE sw.searchTerm = :search_term AND sw.ttl >= :ttl'
			)
			->setParameter('search_term', $searchName)
			->setParameter('ttl', $date)
			->setMaxResults(1)
			->getOneOrNullResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	}

	/**
	 * @param $searchName
	 * @return int
	 */
	public function addSearchWord($searchName)
	{

		// Check if it exists first
		$searchWord = $this->getEntityManager()
				->getRepository('SearchBundle\Entity\SearchWords')
				->findOneBy(['searchTerm' => $searchName]);

		// exist
		if ($searchWord)
		{
			$date = new \DateTime();
			// update ttl
			$searchWord->setTtl($date);
		}
		else
		{
			// No, created one
			$searchWord = new SearchWords();
			$searchWord->setSearchTerm($searchName);
		}

		$em = $this->getEntityManager();
		$em->persist($searchWord);
		$em->flush();

		return $searchWord->getId();

	}


}