<?php

namespace SearchBundle\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class DefaultController
 * @package SearchBundle\Controller
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
	   return $this->render('SearchBundle:Default:index.html.twig');
    }

	/**t
	 * this controller call to service manager to get the results
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function formSearchAction()
	{

		$post = Request::createFromGlobals();

		if ($post->request->has('submit')) {
			$search = $post->request->get('search');
		} else {
			$search = '';
		}

		$engineManager = $this->get('engine.manager');
		$results = $engineManager->getResults($search);

		return $this->render('SearchBundle:Default:index.html.twig', ['search' => $search, 'searchResults' => $results]);
	}
}
