<?php
namespace SearchBundle\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class SearchEngineCompilerPass
 * this class help us to get all the services that tagged as 'search.engine'
 * and send this services ids to the constructor of the SearchEngineManager class
 * @package SearchBundle\Compiler
 */
class SearchEngineCompilerPass implements CompilerPassInterface
{
	/**
	 * @param ContainerBuilder $container
	 */
	public function process(ContainerBuilder $container)
	{
		$services = [];
		$definition = $container->getDefinition('engine.manager');

		$tagged_services = array_keys($container->findTaggedServiceIds('search.engine'));

		foreach ($tagged_services as $id) {
			$services[] = $id;
		}

		$definition->replaceArgument(0, $services);
	}
}