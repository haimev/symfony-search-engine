<?php

namespace SearchBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchEngineManager
 * this class manage the services engine, call them to get the results
 * and save the results in DB
 * @package SearchBundle\Services
 */
class SearchEngineManager
{
	/**
	 * @var
	 */
	private $ttl;

	/**
	 * @var EntityManager
	 */
	private $entityManager;

	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * @var array
	 */
	private $servicesIds;

	/**
	 * SearchEngineManager constructor.
	 * @param $servicesIds
	 * @param ContainerInterface $container
	 * @param EntityManager $entityManager
	 * @param $ttl
	 */
	public function __construct($servicesIds, ContainerInterface $container, EntityManager $entityManager, $ttl)
	{
		$this->container = $container;
		$this->servicesIds  = $servicesIds;
		$this->ttl = $ttl;
		$this->entityManager = $entityManager;
	}

	/**
	 * @param $searchWord
	 * @return array
	 */
	public function getResults($searchWord)
	{

		$existsInDB = $this->entityManager
					->getRepository('SearchBundle:SearchWords')
					->findSearchTermByTtl($searchWord, $this->ttl);

		if (!$existsInDB) {

			// get results from services
			$results = [];

			foreach ($this->servicesIds as $id)	{
				// get service
				$service = $this->container->get($id);

				// check that the service implement the interface
				if (!in_array('SearchBundle\Services\SearchEngineInterface', class_implements($service))) {
					throw new \RuntimeException('Service ' . $id .' not implement the interface');
				}

				// get result by search method
				$serviceResult = $service->search($searchWord);

				$results = array_merge($results, $serviceResult);
			}

			// unique results by link
			$uniqueResults = [];
			foreach ($results as &$v) {
				// if not have title or link skip
				if (empty($v['link']) || empty($v['title']))
					continue;

				if (!isset($uniqueResults[$v['link']]))
					$uniqueResults[$v['link']] =& $v;
			}

			// insert to db searchWord or update ttl in case of exist
			$searchId = $this->entityManager
				->getRepository('SearchBundle:SearchWords')
				->addSearchWord($searchWord);


			// delete and insert results to db
			$this->entityManager
				->getRepository('SearchBundle:SearchResults')
				->insertResults($searchId, $uniqueResults);

		}
		else {
			// return results from db
			$uniqueResults = $this->entityManager
				->getRepository('SearchBundle:SearchResults')
				->getResultsBySearchName($searchWord);
		}

		return $uniqueResults;
	}
}
