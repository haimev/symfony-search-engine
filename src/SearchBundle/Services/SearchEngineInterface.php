<?php

namespace SearchBundle\Services;

/**
 * Interface SearchEngineInterface
 * @package SearchBundle\Services
 */
interface SearchEngineInterface
{
	/**
	 * @param $query
	 * @return array
	 */
	public function search($query);

}
