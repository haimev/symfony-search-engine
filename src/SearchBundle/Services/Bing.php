<?php

namespace SearchBundle\Services;

/**
 * Class Bing
 * the class that call the bing api and return the result according to the query string
 * @package SearchBundle\Services
 */
class Bing implements SearchEngineInterface
{
	/**
	 * Your Bing API key
	 * @var string $apiKey
	 */
	protected $apiKey;

	/**
	 * The base URL for the bing API.
	 *
	 * @var string $apiUrl
	 */
	protected $apiUrl = 'https://api.datamarket.azure.com/Bing/Search/v1/Web';

	/**
	 * Bing constructor.
	 * @param $apiKey
	 */
	public function __construct($apiKey)
	{
		$this->apiKey = $apiKey;
	}

	/**
	 * Performs the most basic of searches. Pass in the query and it returns the
	 * bing search result.
	 *
	 * @param string $query The search string.
	 * @return array [link, title]
	 */
	public function search($query)
	{
		$query = urlencode($query);
		return $this->getResults("&Query='{$query}'");
	}

	/*/**
	 * Returns the constructed API base URL based on the stored parameters.
	 *
	 * @return string The API base url
	 */
	private function getBaseUrl() {
		return $this->apiUrl .
				'?$format=json';
	}

	/**
	 * Returns the results from the custom search API as a array.
	 *
	 * @param string $querystring The full querystring for the bing API.
	 * @throws \RuntimeException if the request fails for any reason.
	 * @return array
	 */
	protected function getResults($querystring) {

		$requestUrl = $this->getBaseUrl() . $querystring;

		if (($ch = curl_init($requestUrl)) === false) {
			throw new \RuntimeException('Unable to initialize request url.');
		}

		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD,  $this->apiKey . ":" . $this->apiKey);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		if (($response = curl_exec($ch)) === false) {
			curl_close($ch);
			throw new \RuntimeException('Unable to execute request.');
		}

		$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if ($responseCode != 200) {
			throw new \RuntimeException('Bing API did not return a valid result: '.$responseCode.' Response: '.$response);
		}

		$response = json_decode($response, true);

		$results = [];
		if (!empty($response['d']['results'])) {
			foreach ($response['d']['results'] as $result) {
				$results[] = ['link' => $result['Url'], 'title' => $result['Title']];
			}
		}

		return $results;
	}

}
