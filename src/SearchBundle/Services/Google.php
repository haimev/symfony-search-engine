<?php

namespace SearchBundle\Services;

/**
 * Class Google
 * the class that call the google api and return the result according to the query string
 * @package SearchBundle\Services
 */
class Google implements SearchEngineInterface
{
	/**
	 * The custom search engine id (cx) that can be found from the Custom Search
	 * home: https://www.google.com/cse/all . Click on the custom search engine
	 * then click on the Search engine ID button.
	 *
	 * @var string $searchEngineId
	 */
	private $searchEngineId;

	/**
	 * Your Google API key which is found in your Developers Console:
	 * https://console.developers.google.com/project . Click on the project then
	 * APIs & auth -> Credentials. Please note, this API key is tied to your
	 * specified IP addresses.
	 *
	 * @var string $apiKey
	 */
	private $apiKey;

	/**
	 * The base URL for the google API.
	 *
	 * @var string $apiUrl
	 */
	private $apiUrl = 'https://www.googleapis.com/customsearch/v1';

	/**
	 * Google constructor.
	 * @param $searchEngineId
	 * @param $googleApiKey
	 */
	public function __construct($searchEngineId, $googleApiKey)
	{
		$this->searchEngineId = $searchEngineId;
		$this->apiKey = $googleApiKey;
	}

	/**
	 * Performs the most basic of searches. Pass in the query and it returns the
	 * Google search result.
	 *
	 * @param string $query The search string.
	 * @return array [link, title]
	 */
	public function search($query) {
		$query = urlencode($query);
		return $this->getResults("&q={$query}");
	}

	/**
	 * Returns the constructed API base URL based on the stored parameters.
	 *
	 * @return string The API base url
	 */
	private function getBaseUrl() {
		return $this->apiUrl .
		"?key={$this->apiKey}" .
		"&cx={$this->searchEngineId}";
	}

	/**
	 * Returns the results from the custom search API as a array.
	 *
	 * @param string $querystring The full querystring for the google API.
	 * @throws \RuntimeException if the request fails for any reason.
	 * @return array
	 */
	private function getResults($querystring) {
		$requestUrl = $this->getBaseUrl() . $querystring;

		if (($ch = curl_init($requestUrl)) === false) {
			throw new \RuntimeException('Unable to initialize request url.');
		}

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		if (($response = curl_exec($ch)) === false) {
			curl_close($ch);
			throw new \RuntimeException('Unable to execute request.');
		}

		$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if ($responseCode != 200) {
			throw new \RuntimeException('Google API did not return a valid result: '.$responseCode.' Response: '.$response);
		}

		$response = json_decode($response, true);
		$results = [];

		if (!empty($response['items'])) {
			foreach ($response['items'] as $result)	{
				$results[] = ['link' => $result['link'], 'title' => $result['title']];
			}
		}


		return $results;
	}
}
