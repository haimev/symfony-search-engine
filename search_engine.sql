DROP TABLE IF EXISTS `search_results`;

CREATE TABLE `search_results` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `search_words_id` int(10) unsigned DEFAULT NULL,
  `link` varchar(2000) NOT NULL,
  `title` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_search_results_search_words` (`search_words_id`),
  CONSTRAINT `fk_search_results_search_words` FOREIGN KEY (`search_words_id`) REFERENCES `search_words` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `search_words`;

CREATE TABLE `search_words` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `search_term` varchar(200) NOT NULL,
  `ttl` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `search_term` (`search_term`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
