Search Engine
===========

## Description
A Symfony 3 project that perform simultaneous search requests to different search engines
and show aggregated result without duplicates.

The results cached in database with specified TTL, you can configure this TTL as described bellow.

Engines that currently support:
* Google
* Bing

## Installation 
The system based on standard Symfony 3.1 without special vendors, you can follow the Symfony documents
http://symfony.com/doc/current/book/installation.html 

you'll need to configure your database connection information in an app/config/parameters.yml
Important : run the sql file search_engine.sql to get the DB schema.

Another parameters need to set in config:

```yml
    # ttl in seconds
    ttl : 600
    # bing
	bing_api_key: put_key_here
	# google
	google_engine_id: put_id_here
	google_api_key: put_key_here
```

## Add another engine
To add another search engine, all you need is to write class that implement the interface SearchEngineInterface.
The search method get string and return an array of result, the array need to contain the keys link and title
When you add the new service need to add the tag name search.engine
If the class need another parameters like api key add them to the parameters.yml and pass them to the constructor
like:

```yml
  engine.engineName:
    class:        SearchBundle\Services\engineName
    arguments:    [%api_key%]
    tags:
        -  { name: search.engine }
```

## Authors

Haim Evgy - <haimev@gmail.com>